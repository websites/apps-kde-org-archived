#!/usr/bin/python3
# Copyright 2016 Boudhayan Gupta <bgupta@kde.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. Neither the name of KDE e.V. (or its successor approved by the
#    membership of KDE e.V.) nor the names of its contributors may be used
#    to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import tornado.ioloop
import tornado.web

import api
import visual

def akoApplicationFactory():
    return tornado.web.Application([
        (r"/api/?", tornado.web.RedirectHandler, { "url" : "https://community.kde.org/Sysadmin/Project_Metadata_API" }),
        (r"/api/v1/index/([\w\-/]+?)/?", api.ProjectList),
        (r"/api/v1/projects/?", api.MultipleProjects),
        (r"/api/v1/components/?", api.ComponentList),
        (r"/api/v1/repo/([\w\-]+)/?", api.SingleProjectByRepo),
        (r"/api/v1/project/([\w\-/]+?)/?", api.SingleProjectByPath),
        (r"/", tornado.web.RedirectHandler, { "url" : "https://www.kde.org/applications/" })
    ])

if __name__ == "__main__":
    app = akoApplicationFactory()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()
