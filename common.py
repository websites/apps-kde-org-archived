# Copyright 2016 Boudhayan Gupta <bgupta@kde.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. Neither the name of KDE e.V. (or its successor approved by the
#    membership of KDE e.V.) nor the names of its contributors may be used
#    to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import os
import copy
import yaml
import git

from fnmatch import fnmatch
from functools import reduce
from collections import OrderedDict

try:
    import simplejson as json
except ImportError:
    import json

def getMetaByRepo(repo):
    with open("config.json") as f:
        config = json.load(f)
    indexfile = os.path.join(config["metapath"], "output", "repoindex.json")

    if not os.path.isfile(indexfile):
        return None
    with open(indexfile) as f:
        index = json.load(f)

    path = index.get(repo)
    if not path:
        return None
    return genMetadata(path)

def genProjectTree(root, base):
    subdirs = []
    for entry in os.scandir(root):
        if entry.is_dir():
            subdirs.append(entry.name)
    if subdirs:
        level = OrderedDict.fromkeys(subdirs)
        for k in level.keys():
            level[k] = genProjectTree(os.path.join(root, k), base)
        return level
    return root[len(base) + 1:]

def genMetadata(project):
    # begin by loading our configuration
    with open("config.json") as f:
        config = json.load(f)

    # check if the project even exists
    projectdir = os.path.join(config["metapath"], "projects", project)
    projectdir = os.path.abspath(projectdir)
    if not os.path.isdir(projectdir):
        return None

    # check if the project has a valid metafile
    metadatafile = os.path.join(projectdir, "metadata.yaml")
    if not os.path.isfile(metadatafile):
        return None

    # init our data structures
    metadata = OrderedDict([
        ("name", None),
        ("description", None),
        ("maintainers", []),
        ("projectPath", None),
        ("webLinks", {}),
        ("repository", { "hasRepo" : False })
    ])

    repometa = OrderedDict([
        ("hasRepo", True),
        ("isActive", False),
        ("repoType", None),
        ("repoUrls", {}),
        ("i18nBranches", {}),
        ("allBranches", []),
    ])

    # read the data and populate the metadata
    with open(metadatafile) as f:
        metafile = yaml.load(f)

    # basic data
    metadata["name"] = metafile["name"]
    metadata["description"] = metafile["description"]
    metadata["maintainers"] = metafile["members"]
    metadata["projectPath"] = project

    # add in the web link to the html version of this resource
    appspath = os.path.normpath(project)
    metadata["webLinks"]["appsdb"] = "https://{0}/projects/{1}".format(config["domain"], appspath)

    # if we have a repository, we'll need to populate that too
    if metafile["hasrepo"]:
        metacfgpath = os.path.join(config["metapath"], "config")

        # basic repo information
        repometa["isActive"] = metafile["repoactive"]
        repometa["repoType"] = "git"

        # links to access the repository
        with open(os.path.join(metacfgpath, "urls_gitrepo.json")) as f:
            repourls = json.load(f)
        for k in repourls.keys():
            repometa["repoUrls"][k] = repourls[k]
            repometa["repoUrls"][k]["url"] = repometa["repoUrls"][k]["url"].format(metafile["repopath"])

        # access to the gitweb interface
        with open(os.path.join(metacfgpath, "urls_webaccess.json")) as f:
            weburls = json.load(f)
        for k in weburls.keys():
            metadata["webLinks"][k] = weburls[k].format(metafile["repopath"])

        # i18n branches information
        with open(os.path.join(metacfgpath, "i18n_defaults.json")) as f:
            i18ndefaults = json.load(f, object_pairs_hook = OrderedDict)
        for k in i18ndefaults.keys():
            if fnmatch(appspath, k):
                repometa["i18nBranches"] = copy.deepcopy(i18ndefaults[k])
        if not repometa["i18nBranches"]:
            repometa["i18nBranches"] = { "trunk" : "none", "stable" : "none", "trunk_kf5" : "none", "stable_kf5" : "none" }

        # i18n branches overrides
        i18novrfile = os.path.join(projectdir, "i18n.json")
        if os.path.isfile(i18novrfile):
            with open(i18novrfile) as f:
                i18noverrides = json.load(f)
            for k in i18noverrides.keys():
                repometa["i18nBranches"][k] = i18noverrides[k]

        # all branches information
        with open(os.path.join(metacfgpath, "common.json")) as f:
            metacfg = json.load(f)
        repopath = os.path.join(metacfg["repostore"], metafile["repopath"]) + ".git"
        try:
            repo = git.Repo(repopath)
            for branch in repo.branches:
                repometa["allBranches"].append(branch.name)
        except Exception as exc:
            print("error: no git repository at {0}. unable to get branches.".format(repopath))

        # done with repo information
        metadata["repository"] = repometa

    # done
    return metadata
