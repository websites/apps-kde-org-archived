# Copyright 2016 Boudhayan Gupta <bgupta@kde.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. Neither the name of KDE e.V. (or its successor approved by the
#    membership of KDE e.V.) nor the names of its contributors may be used
#    to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import tornado.web
import os
import common

try:
    import simplejson as json
except ImportError:
    import json

class ComponentList(tornado.web.RequestHandler):
    def get(self):
        self.set_header("Content-Type", "application/json")
        with open("config.json") as f:
            config = json.load(f)
        projectdir = os.path.join(config["metapath"], "projects")
        self.write(json.dumps(os.listdir(projectdir)))

class ProjectList(tornado.web.RequestHandler):
    def get(self, subpath = None):
        self.set_header("Content-Type", "application/json")

        with open("config.json") as f:
            config = json.load(f)
        projectdir = os.path.join(config["metapath"], "projects")
        if subpath:
            projectdir = os.path.join(projectdir, subpath)

        if not os.path.isdir(projectdir):
            self.set_status(404)
            self.write(json.dumps({
                "error" : 404,
                "message" : "No such project path: {0}".format(subpath)
            }))
            return

        tree = common.genProjectTree(projectdir, os.path.join(config["metapath"], "projects"))
        if type(tree) is str:
            tree = [tree]
        self.write(json.dumps(tree))

class SingleProjectByPath(tornado.web.RequestHandler):
    def get(self, project):
        self.set_header("Content-Type", "application/json")

        metadata = common.genMetadata(project)
        if not metadata:
            self.set_status(404)
            self.write(json.dumps({
                "error" : 404,
                "message" : "No such project: {0}".format(project)
            }))
            return
        self.write(metadata)

class SingleProjectByRepo(tornado.web.RequestHandler):
    def get(self, repo):
        self.set_header("Content-Type", "application/json")

        metadata = common.getMetaByRepo(repo)
        if not metadata:
            self.set_status(404)
            self.write(json.dumps({
                "error" : 404,
                "message" : "No such repository: {0}".format(repo)
            }))
            return
        self.write(metadata)

class MultipleProjects(tornado.web.RequestHandler):
    def post(self):
        self.set_header("Content-Type", "application/json")

        paths = []
        try:
            paths.extend(json.loads(self.request.body.decode("utf-8")))
        except json.JSONDecodeError as exc:
            self.set_status(400)
            self.write(json.dumps({
                "error": 400,
                "message": "Invalid JSON request body"
            }))
            return

        metas = {}
        for path in paths:
            meta = common.genMetadata(path)
            if not meta:
                self.set_status(404)
                self.write(json.dumps({
                    "error" : 404,
                    "message" : "No such project: {0}".format(path)
                }))
                return
            metas[path] = meta
        self.write(json.dumps(metas))

class RepoIndex(tornado.web.RequestHandler):
    def get(self):
        self.set_header("Content-Type", "application/json")
        with open("config.json") as f:
            config = json.load(f)
        projectdir = os.path.join(config["metapath"], "projects")
        mapping = common.indexByRepo(projectdir, projectdir)
        self.write(json.dumps(mapping))
